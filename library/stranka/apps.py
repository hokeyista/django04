from django.apps import AppConfig


class StrankaConfig(AppConfig):
    name = 'stranka'
